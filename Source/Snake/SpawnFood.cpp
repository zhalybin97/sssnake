// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawnFood.h"
#include "Food.h"
#include "EatableSurprise.h"

// Sets default values
ASpawnFood::ASpawnFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ASpawnFood::BeginPlay()
{
	Super::BeginPlay();
	//Min = FVector2D(-100,100);
	//Max = FVector2D(-100,100);
	RandomSpawn();
}

// Called every frame
void ASpawnFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnFood::Spawn()
{
	FMath a;
	FVector Loc((int)a.RandRange(Min.X,Max.X), (int)a.FRandRange(Min.Y, Max.Y),0);
	FTransform Trans(FVector(0,0,0));
	AFood* food = GetWorld()->SpawnActor<AFood>(foodClass, Trans);
	food->GridCoordinates = Loc;
	food->GridStep = 60;
	food->FoodSpawner = this;
	food->PlaceOnGrid(food->GridStep);
}

void ASpawnFood::SpawnEatable()
{
	FMath a;
	FVector Loc((int)a.RandRange(Min.X, Max.X), (int)a.FRandRange(Min.Y, Max.Y), 0);
	FTransform Trans(FVector(0, 0, 0));
	AEatableSurprise* food = GetWorld()->SpawnActor<AEatableSurprise>(surpriseClass, Trans);
	food->GridCoordinates = Loc;
	food->GridStep = 60;
	food->FoodSpawner = this;
	food->PlaceOnGrid(food->GridStep);
}

void ASpawnFood::RandomSpawn()
{
	FMath a;
	switch (a.RandRange(0,9)) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
			Spawn();
			break;
		case 9:
			SpawnEatable();
			break;
	}
}


