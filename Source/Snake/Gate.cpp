// Fill out your copyright notice in the Description page of Project Settings.


#include "Gate.h"
#include "ButtonPlate.h"

AGate::AGate()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGate::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(0.5f);
	IsOpen = false;
	for (auto i : Buttons)
		GateKeys.Add(false);
}

void AGate::Tick(float DeltaTime)
{
	GateMove();
	CloseGate();
}

void AGate::GateMove()
{
	if (IsOpen && (MoveList.Num() != 0))
	{
		FVector a = MoveList.Pop();
		GridCoordinates += a;
		ResetLocationOnGrid();
		RemoveList.Push(a);
	}
}

void AGate::SetMoveList(TArray<FVector>* NewMoveList)
{
	MoveList = *NewMoveList;
}

void AGate::SetKey(int32 KeyIndex)
{
	GateKeys[KeyIndex] = !GateKeys[KeyIndex];
	bool a = true;
	for (int32 i = 0; i < GateKeys.Num(); i++) {
		a &= GateKeys[i];
	}
	IsOpen = a;
}

void AGate::CloseGate()
{
	if (!IsOpen && (RemoveList.Num() != 0))
	{
		FVector a = RemoveList.Pop();
		GridCoordinates -= a;
		ResetLocationOnGrid();
		MoveList.Push(a);
	}
}

