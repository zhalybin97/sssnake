	// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "GlobalGameState.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BonusSpeed = 0;
	LastMovementDirection = EMovementDirection::STAY;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	auto Actors = GetWorld()->GetCurrentLevel()->Actors;
	for (auto i : Actors)
	{
		AGlobalGameState* a = Cast<AGlobalGameState>(i);
		if (a) {
			GGS = a;
			ElementSize = GGS->GridStep;
			break;
		}
	}
	SetActorTickInterval(GGS->GameSpeed);
	GGS->Snake = this;
	LastTailPosition = FVector(0, 0, 0);
	AddSnakeElement(4);
	isAlive = true;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(isAlive)
		Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++) {
		FVector NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
		AddElementAfterMeal(NewLocation);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}

	if (LastMovementDirection != EMovementDirection::STAY) {
		//AddActorWorldOffset(MovementVector);
		SnakeElements[0]->ToggleCollision();

		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			auto CurrentElement = SnakeElements[i];
			auto PrevElement = SnakeElements[i - 1];
			FVector PrevLocation = PrevElement->GetActorLocation();
			if (i == SnakeElements.Num() - 1) {
				LastTailPosition = CurrentElement->GetActorLocation();
			}

			CurrentElement->SetActorLocation(PrevLocation);

		}

		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		SnakeElements[0]->ToggleCollision();
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SetSpeed()
{
	SetActorTickInterval(GGS->GameSpeed + BonusSpeed);
}

void ASnakeBase::AddElementAfterMeal(FVector SetLocation)
{
	FVector NewLocation = SetLocation;
	FTransform NewTransform = FTransform(NewLocation);

	ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	NewSnakeElement->SnakeOwner = this;
	int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
	NewSnakeElement->Index = ElementIndex;

	if (ElementIndex == 0) {
		NewSnakeElement->SetFirstElementType();
	}
}

void ASnakeBase::DestroySnake()
{
	for (int i = SnakeElements.Num() - 1; i > -1; i--) {
		auto a = SnakeElements[i];
		SnakeElements.Remove(a);
		a->Destroy();
	}
}

