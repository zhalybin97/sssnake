// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "EditorGridPlaceble.h"
#include "EatableSurprise.generated.h"

class ASpawnFood;
/**
 * 
 */
UCLASS()
class SNAKE_API AEatableSurprise : public AEditorGridPlaceble, public IInteractable
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AEatableSurprise();
	ASpawnFood* FoodSpawner;

	float Time;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
