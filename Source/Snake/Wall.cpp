// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "Math.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "GlobalGameState.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::ChangeSize(FVector NewWidthHeight)
{
	WidthHeight = NewWidthHeight;
	int8 SignX = signbit(WidthHeight.X) ? -1 : 1;
	int8 SignY = signbit(WidthHeight.Y) ? -1 : 1;

	FVector temp((50 * SignX + GridStep * (NewWidthHeight.X - SignX)) / 100, (50 * SignY + GridStep * (NewWidthHeight.Y - SignY)) / 100, 0.5f);
	this->SetActorRelativeScale3D(temp);
	ResetLocationOnGrid();
}

void AWall::SetLocationOnGrid(FVector NewLocation)
{
	int8 SignX = signbit(WidthHeight.X) ? -1 : 1;
	int8 SignY = signbit(WidthHeight.Y) ? -1 : 1;
	GridCoordinates = NewLocation;
	NewLocation = PlaceOnGrid(GridStep);
	FVector LocFromScale((WidthHeight.X - SignX) * (GridStep / 2), (WidthHeight.Y - SignY) * (GridStep / 2), 0);
	NewLocation += LocFromScale;
	this->SetActorLocation(NewLocation);
}

void AWall::SetLocationOnGridFromDelta(int32 NewWidth, int32 NewHeight)
{
	GridCoordinates.X += NewWidth;
	GridCoordinates.Y += NewHeight;
	ResetLocationOnGrid();
}

void AWall::ResetLocationOnGrid()
{
	SetLocationOnGrid(GridCoordinates);
}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	GGS->Snake->isAlive = false;
	GGS->Snake->DestroySnake();
	GGS->Snake->AddSnakeElement(4);
	GGS->Snake->BonusSpeed = 0;
	GGS->Snake->SetSpeed();
	GGS->Snake->LastMovementDirection = EMovementDirection::STAY;
	GGS->Snake->isAlive = true;
}

