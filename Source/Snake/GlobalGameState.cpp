// Fill out your copyright notice in the Description page of Project Settings.


#include "GlobalGameState.h"

// Sets default values
AGlobalGameState::AGlobalGameState()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGlobalGameState::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGlobalGameState::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGlobalGameState::GamePause()
{
	GameSpeed = 0;
}

void AGlobalGameState::GameResume()
{
	GameSpeed = DefaultGameSpeed;
}

