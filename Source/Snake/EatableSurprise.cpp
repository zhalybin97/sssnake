// Fill out your copyright notice in the Description page of Project Settings.


#include "EatableSurprise.h"
#include "SnakeBase.h"
#include "SpawnFood.h"

AEatableSurprise::AEatableSurprise() {
	Time = 5;
}

void AEatableSurprise::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Time -= DeltaTime;
	if (Time<=0) {
		FoodSpawner->RandomSpawn();
		this->Destroy();
	}
}

void AEatableSurprise::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (Snake) {
			Snake->BonusSpeed -= Snake->BonusSpeed*0.1f;
			Snake->SetSpeed();
			FoodSpawner->RandomSpawn();
			this->Destroy();
		}
	}
}

void AEatableSurprise::BeginPlay()
{
}
