// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EditorGridPlaceble.h"
#include "ButtonPlate.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class AGate;
/**
 * 
 */
UCLASS()
class SNAKE_API AButtonPlate : public AEditorGridPlaceble
{

	GENERATED_BODY()

public:
	AButtonPlate();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<UMaterial*> Materials;

	bool ButtonState;
	
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxComponent;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		AGate* TargetGate;
	UPROPERTY(EditAnywhere)
		int32 KeyIndex;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Push(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepHitResult);
};
