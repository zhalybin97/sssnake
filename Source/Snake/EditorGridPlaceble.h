// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EditorGridPlaceble.generated.h"

class AGlobalGameState;

UCLASS()
class SNAKE_API AEditorGridPlaceble : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AEditorGridPlaceble();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FVector GridCoordinates;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 GridStep;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		AGlobalGameState* GGS;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		FVector PlaceOnGrid(float Step = 1);

	UFUNCTION(BlueprintCallable)
	void GetGlobalGameState();
};
