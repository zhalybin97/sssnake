// Fill out your copyright notice in the Description page of Project Settings.


#include "ButtonPlate.h"
#include "Gate.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

AButtonPlate::AButtonPlate()
{
	PrimaryActorTick.bCanEverTick = true;
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxColision"));
	RootComponent = BoxComponent;
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void AButtonPlate::BeginPlay()
{
	Super::BeginPlay();
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AButtonPlate::Push);
	ButtonState = false;
}

// Called every frame
void AButtonPlate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UFUNCTION()
void AButtonPlate::Push(UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepHitResult)
{
	auto a = Cast<ASnakeElementBase>(OtherActor);
	if(IsValid(a))
		if (a->IsHead)
		{
			MeshComponent->SetMaterial(0, Materials[!ButtonState]);
			ButtonState = !ButtonState;
			TargetGate->SetKey(KeyIndex);
		}
}

