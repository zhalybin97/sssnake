// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnFood.generated.h"

class AFood;
class AEatableSurprise;

UCLASS()
class SNAKE_API ASpawnFood : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnFood();

	UPROPERTY(EditAnywhere)
	TSubclassOf<AFood> foodClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AEatableSurprise> surpriseClass;

	UPROPERTY(EditAnywhere)
	FVector2D Min;
	UPROPERTY(EditAnywhere)
	FVector2D Max;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Spawn();
	void SpawnEatable();

	void RandomSpawn();
};