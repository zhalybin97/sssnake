// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EditorGridPlaceble.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Wall.generated.h"

class UStaticMeshComponent;
class AGlobalGameState;

UCLASS()
class SNAKE_API AWall : public AEditorGridPlaceble, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	

	UPROPERTY()
		FVector WidthHeight;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void ChangeSize(FVector NewWidthHeight);
	UFUNCTION(BlueprintCallable)
		void SetLocationOnGrid(FVector NewWidthHeight);
	UFUNCTION(BlueprintCallable)
		void SetLocationOnGridFromDelta(int32 NewWidth, int32 NewHeight);
	UFUNCTION(BlueprintCallable)
		void ResetLocationOnGrid();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
