// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.h"
#include "GlobalGameState.generated.h"

UCLASS()
class SNAKE_API AGlobalGameState : public AActor
{
	GENERATED_BODY()
private:
	int32 DefaultGameSpeed;
	
public:	
	// Sets default values for this actor's properties
	AGlobalGameState();

	UPROPERTY(EditAnywhere)
	float GameSpeed;
	UPROPERTY(EditAnywhere)
	float GridStep;
	UPROPERTY(EditAnywhere)
	ASnakeBase* Snake;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void GamePause();
	void GameResume();

};
