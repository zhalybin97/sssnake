// Fill out your copyright notice in the Description page of Project Settings.


#include "EditorGridPlaceble.h"
#include "GlobalGameState.h"

// Sets default values
AEditorGridPlaceble::AEditorGridPlaceble()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEditorGridPlaceble::BeginPlay()
{
	Super::BeginPlay();
	GetGlobalGameState();
}

// Called every frame
void AEditorGridPlaceble::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AEditorGridPlaceble::PlaceOnGrid(float Step)
{
	FVector a(GridCoordinates.X * Step, GridCoordinates.Y * Step, 0);
	this->SetActorLocation(a);
	return a;
}

void AEditorGridPlaceble::GetGlobalGameState()
{
	auto Actors = GetWorld()->GetCurrentLevel()->Actors;
	for (auto i : Actors)
	{
		AGlobalGameState* a = Cast<AGlobalGameState>(i);
		if (a) {
			GGS = a;
			GridStep = GGS->GridStep;
			break;
		}
	}
}

