// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Wall.h"
#include "Gate.generated.h"

class AButtonPlate;
/**
 * 
 */

UCLASS()
class SNAKE_API AGate : public AWall
{
	GENERATED_BODY()
public:
	AGate();

	UPROPERTY(EditAnywhere)
		TArray<FVector> MoveList;
	UPROPERTY(EditAnywhere)
		TArray<FVector> RemoveList;
	UPROPERTY(EditAnywhere)
	TArray<bool> GateKeys;
	UPROPERTY(EditAnywhere)
	TArray<AButtonPlate*> Buttons;

	bool IsOpen;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void GateMove();
	void SetMoveList(TArray<FVector>* NewMoveList);
	void SetKey(int32 KeyIndex);
	void SetKey(int32 KeyIndex, bool Value);
	void CloseGate();
};
